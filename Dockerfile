FROM myparcel/base

ENV PHP_VERSION 7.1

ARG DEBIAN_FRONTEND=noninteractive

COPY etc/apt /etc/apt

# Mount /tmp and /var/lib/apt/lists as tmpfs to prevent any files in those dirs leaking into the image.
RUN --mount=type=tmpfs,target=/tmp \
    --mount=type=tmpfs,target=/var/lib/apt/lists \
    apt-get --yes --quiet update \
 && apt-get --yes --quiet install --no-install-recommends software-properties-common wget \
    # Add NGiNX repository
 && add-apt-repository --yes ppa:nginx/stable \
    # Add PHP repository
 && add-apt-repository --yes ppa:ondrej/php \
 && apt-get --yes --quiet update \
 && apt-get --yes --quiet install --no-install-recommends \
        acl \
        blackfire-php \
        blackfire-agent \
        git \
        newrelic-php5 \
        nginx \
        openssh-server \
        patch \
        pdftk \
        php${PHP_VERSION}-apcu \
        php${PHP_VERSION}-bcmath \
        php${PHP_VERSION}-cli \
        php${PHP_VERSION}-curl \
        php${PHP_VERSION}-dom \
        php${PHP_VERSION}-fpm \
        php${PHP_VERSION}-gd \
        php${PHP_VERSION}-gmp \
        php${PHP_VERSION}-iconv \
        php${PHP_VERSION}-intl \
        php${PHP_VERSION}-json \
        php${PHP_VERSION}-mbstring \
        php${PHP_VERSION}-mcrypt \
        php${PHP_VERSION}-opcache \
        php${PHP_VERSION}-pdo \
        php${PHP_VERSION}-pdo-pgsql \
        php${PHP_VERSION}-pgsql \
        php${PHP_VERSION}-posix \
        php${PHP_VERSION}-redis \
        php${PHP_VERSION}-simplexml \
        php${PHP_VERSION}-soap \
        php${PHP_VERSION}-xdebug \
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-xmlreader \
        php${PHP_VERSION}-xmlwriter \
        php${PHP_VERSION}-zip \
        postgresql-client \
        redis-tools \
        ssl-cert \
        unzip \
 && apt-get --yes --quiet clean \
 && rm --recursive --force /var/lib/apt/lists/*

ENV COMPOSER_ALLOW_SUPERUSER="1"

ARG COMPOSER_NO_INTERACTION="1"
ARG COMPOSER_MIN_VERSION="2.0.0"

RUN --mount=type=tmpfs,target=/tmp \
    expected_checksum="$(php --run "echo file_get_contents('https://composer.github.io/installer.sig');")" \
 && php --run "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
 && actual_checksum="$(php --run "echo hash_file('sha384', 'composer-setup.php');")" \
 && test "${expected_checksum}" = "${actual_checksum}" || (echo "composer-setup.php checksum failed"; exit 1) \
 && php composer-setup.php --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_MIN_VERSION} \
 && php --run "unlink('composer-setup.php');" \
 && /usr/local/bin/composer --no-ansi self-update --clean-backups --no-progress "--${COMPOSER_MIN_VERSION%%.*}"

COPY etc/php/conf.d/*        /etc/php/${PHP_VERSION}/cli/conf.d/
COPY etc/php/conf.d/*        /etc/php/${PHP_VERSION}/fpm/conf.d/
COPY etc/php/fpm/pool.d/*    /etc/php/${PHP_VERSION}/fpm/pool.d/
COPY etc/php/xdebug-conf.d/* /usr/local/etc/php/xdebug-conf.d/

RUN --mount=type=tmpfs,target=/tmp \
    cp /etc/newrelic/newrelic.cfg.template /etc/newrelic/newrelic.cfg \
 && mkdir --parents /var/run/blackfire \
 && mkdir --parents /var/log/blackfire \
 && mkdir --parents /run/php \
    # Move Xdebug config to separate directory so it's disabled by default but can be optionally enabled.
 && mkdir --parents --verbose /usr/local/etc/php/xdebug-conf.d \
 && mv --verbose "/etc/php/${PHP_VERSION}/fpm/conf.d/20-xdebug.ini" "/usr/local/etc/php/xdebug-conf.d/20-xdebug.ini" \
 && rm --verbose "/etc/php/${PHP_VERSION}/cli/conf.d/20-xdebug.ini"

ENV DBGP_IDEKEY="" \
    NEWRELIC_APP_NAME="PHP Application" \
    NEWRELIC_DAEMON_ADDRESS="/tmp/.newrelic.sock" \
    NEWRELIC_LABELS="" \
    NEWRELIC_LICENSE="" \
    OPCACHE_ENABLE="1" \
    OPCACHE_ENABLE_CLI="0" \
    OPCACHE_ERROR_LOG="stderr" \
    OPCACHE_LOG_VERBOSITY_LEVEL="1" \
    OPCACHE_REVALIDATE_FREQ="2" \
    OPCACHE_VALIDATE_TIMESTAMPS="0" \
    PHP_ASSERT_EXCEPTION="Off" \
    PHP_DISPLAY_ERRORS="Off" \
    PHP_DISPLAY_STARTUP_ERRORS="Off" \
    PHP_ERROR_REPORTING="22527" \
    PHP_FPM_PM_MAX_CHILDREN="65" \
    PHP_MAX_EXECUTION_TIME="300" \
    PHP_MEMORY_LIMIT="1024M" \
    PHP_ZEND_ASSERTIONS="-1" \
    PHP_ZEND_EXCEPTION_IGNORE_ARGS="On" \
    XDEBUG_DEFAULT_ENABLE="0" \
    XDEBUG_REMOTE_HOST="host.docker.internal"
